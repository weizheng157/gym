package com.mercury.member.security.handlers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercury.member.beans.Member;
import com.mercury.member.http.AuthenticationSuccessResponse;
import com.mercury.member.http.Response;


public class SecurityUtil {
	
	private static final ObjectMapper mapper = new ObjectMapper();

    public static void sendResponse(HttpServletResponse httpServletResponse, int status, String message, Exception exception) throws IOException {
       Response response = new Response(exception == null ? true : false, status, message);
        flushResponse(httpServletResponse, response);
    }
    
    public static void sendAuthenticationSuccessResponse(HttpServletResponse httpServletResponse, int status, String message, Exception exception, Member member)
    		throws IOException {
    		Response response = new AuthenticationSuccessResponse(exception == null ? true : false, status, message, member);
    		System.out.println(response);
    		flushResponse(httpServletResponse, response);
    }
    
    public static void flushResponse(HttpServletResponse httpServletResponse, Response response) throws IOException {
    		httpServletResponse.setContentType("application/json;charset=UTF-8");
    		httpServletResponse.setStatus(response.getCode());
        PrintWriter writer = httpServletResponse.getWriter();
        writer.write(mapper.writeValueAsString(response));
        writer.flush();
        writer.close();
    }
	
}
