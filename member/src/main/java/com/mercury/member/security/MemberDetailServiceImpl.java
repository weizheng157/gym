package com.mercury.member.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.mercury.member.beans.Member;
import com.mercury.member.daos.MemberDao;

public class MemberDetailServiceImpl implements UserDetailsService{

	@Autowired
	private MemberDao memberDao;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Member member = (Member) memberDao.findByUsername(username);
		return member;
	}
}
