package com.mercury.member.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.mercury.member.security.handlers.AccessDeniedHandlerImpl;
import com.mercury.member.security.handlers.AuthenticationEntryPointImpl;
import com.mercury.member.security.handlers.AuthenticationFailureHandlerImpl;
import com.mercury.member.security.handlers.AuthenticationSuccessHandlerImpl;
import com.mercury.member.security.handlers.LogoutSuccessHandlerImpl;



@EnableWebSecurity  // let spring security use this configuration file
@EnableGlobalMethodSecurity(prePostEnabled = true)

// Spring Security default login page will be disabled if handler (authenticationEntryPoint) is assigned
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private AuthenticationEntryPointImpl authenticationEntryPointImpl;
	
	@Autowired
	private AccessDeniedHandlerImpl accessDeniedHandlerImpl;
	
	@Autowired
	private AuthenticationSuccessHandlerImpl authenticationSuccessHandlerImpl;
	
	@Autowired
	private AuthenticationFailureHandlerImpl authenticationFailureHandlerImpl;
	
	@Autowired
	private LogoutSuccessHandlerImpl logoutSuccessHandlerImpl;
	
	public void configure(HttpSecurity http) throws Exception {

		http
			.cors()		//by default, cors is disabled
				.and()
			.csrf()		//by default, csrf is enabled
				.disable()
//				.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//				.and()
			.authorizeRequests()
//				.anyRequest().
				.antMatchers("/login").permitAll()
					// "/products/*" you can use products/1. If no *, only can access /products
//				.antMatchers(HttpMethod.GET,"/login").permitAll() 
//				.antMatchers(HttpMethod.GET, "/users").hasRole("ADMIN")
//				.anyRequest().authenticated()
				.and()
			.exceptionHandling()
				.accessDeniedHandler(accessDeniedHandlerImpl)
				.authenticationEntryPoint(authenticationEntryPointImpl)
				.and()
			.formLogin()
				.permitAll()
				.loginProcessingUrl("/login")
				.usernameParameter("username")
				.passwordParameter("password")
				.successHandler(authenticationSuccessHandlerImpl)
				.failureHandler(authenticationFailureHandlerImpl)
				.and()
			.logout()
				.permitAll()	
				.logoutUrl("/logout")
				.logoutSuccessHandler(logoutSuccessHandlerImpl)
				.and()
			.httpBasic();
//			.rememberMe(); // terminal function  
	}
	
	@Bean       // put the return object into spring container, as a bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11); // 加密算法
    }
	
	@Autowired      // @Autowired on function will autowired the parameters
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }
	
	@Bean
    public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
        configuration.addAllowedOrigin("*"); // You should only set trusted site here. e.g. http://localhost:4200 means only this site can access.
        configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","HEAD","OPTIONS"));
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
