package com.mercury.member.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.member.beans.MemberDetail;

public interface MemberDetailDao extends JpaRepository<MemberDetail, Integer> {

}
