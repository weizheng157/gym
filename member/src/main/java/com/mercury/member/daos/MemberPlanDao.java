package com.mercury.member.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.member.beans.MemberPlan;

public interface MemberPlanDao extends JpaRepository<MemberPlan, Integer>{

}