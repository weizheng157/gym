package com.mercury.member.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mercury.member.beans.Member;


public interface MemberDao extends JpaRepository<Member, Integer> {
	public Member findByUsername(String username);

}
