package com.mercury.member.http;

import com.mercury.member.beans.Member;

public class AuthenticationSuccessResponse extends Response{
	
	private Member member;
	
	public AuthenticationSuccessResponse(boolean success, int code, String message, Member member) {
		super(success, code, message);
		this.member = member;
	}
	
	public Member getMember() {
		return member;
	}
	
	public void setMember(Member member) {
		this.member = member;
	}

}
