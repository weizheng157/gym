package com.mercury.member.http;

import com.mercury.member.beans.MemberDetail;

public class MemberDetailResponse extends Response{
	
	private MemberDetail memberDetail;
	
	public MemberDetailResponse(boolean success, MemberDetail memberDetail) {
		super(success);
		this.memberDetail = memberDetail;
	}
	
	public MemberDetail getUsertail() {
		return memberDetail;
	}
	
	public void setUserDetail(MemberDetail memberDetail) {
		this.memberDetail = memberDetail;
	}

}
