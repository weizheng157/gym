package com.mercury.member.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mercury.member.beans.MemberDetail;
import com.mercury.member.daos.MemberDao;
import com.mercury.member.daos.MemberDetailDao;


@Service
public class MemberDetailService {
	@Autowired
	private MemberDetailDao mdd;
	
	@Autowired
	private MemberDao memberDao;
	
	public List<MemberDetail> getAll() {
		
		return mdd.findAll();
	}
}
