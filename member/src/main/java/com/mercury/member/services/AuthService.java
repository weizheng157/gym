package com.mercury.member.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.mercury.member.daos.MemberDao;
import com.mercury.member.http.AuthenticationSuccessResponse;
import com.mercury.member.http.Response;

@Service
public class AuthService {
	
	@Autowired
	MemberDao memberDao;
	
	public Response checklogin(Authentication authentication) {
		if(authentication != null) {
			Response response = new AuthenticationSuccessResponse(true, 200, "Logged In!", memberDao.findByUsername(authentication.getName()));
			return response;
		} else {
			return new Response(false);
		}
	}
}
