package com.mercury.member.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mercury.member.beans.Member;
import com.mercury.member.daos.MemberDao;
import com.mercury.member.http.Response;

@Service
public class MemberService {
	@Autowired
	private MemberDao memberDao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public Member findMember(int id) {
		return memberDao.findById(id).get();
	}
	
	public Member findMemberByName (String name) {
		Member member = memberDao.findByUsername(name);
		return member;
	}
	
	public List<Member> getALl() {
		return memberDao.findAll();
	}
	
	public Response register(Member member) {
		member.setPassword(passwordEncoder.encode(member.getPassword()));
		member.getMemberDetail().setMember(member);
		memberDao.save(member);
		
		return new Response(true);
	}
	
	public Response changePassword(Member member, Authentication authentication) {
		if(member.getUsername().equals(authentication.getName())) {
			Member m = memberDao.findByUsername(member.getUsername());
			member.setPassword(passwordEncoder.encode(member.getPassword()));
			memberDao.save(m);
		} else {
			return new Response(false);
		}
		return new Response(true);
	}
	
	public Response deteleMember(int id) {
		if(memberDao.findById(id).get() != null) {
			memberDao.deleteById(id);
			return new Response(true);
		} else {
			return new Response(false, "Member is not found");
		}
	}
}
