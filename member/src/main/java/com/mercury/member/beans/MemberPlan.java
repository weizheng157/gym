package com.mercury.member.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "MEMBER_PLAN")
public class MemberPlan implements GrantedAuthority{
	@Id
	private int id;
	@Column
	private double price;
	@Column
	private int freeSession;
	@Column
	private int freeTraining;
	@Column
	private String type;
	@Column
	private int sessionPrice;
	@Column
	private int trainingPrice;
	
	public MemberPlan() {
		super();
	}

	public MemberPlan(int id, double price, int freeSession, int freeTraining, String type, int sessionPrice,
			int trainingPrice) {
		super();
		this.id = id;
		this.price = price;
		this.freeSession = freeSession;
		this.freeTraining = freeTraining;
		this.type = type;
		this.sessionPrice = sessionPrice;
		this.trainingPrice = trainingPrice;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getFreeSession() {
		return freeSession;
	}

	public void setFreeSession(int freeSession) {
		this.freeSession = freeSession;
	}

	public int getFreeTraining() {
		return freeTraining;
	}

	public void setFreeTraining(int freeTraining) {
		this.freeTraining = freeTraining;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSessionPrice() {
		return sessionPrice;
	}

	public void setSessionPrice(int sessionPrice) {
		this.sessionPrice = sessionPrice;
	}

	public int getTrainingPrice() {
		return trainingPrice;
	}

	public void setTrainingPrice(int trainingPrice) {
		this.trainingPrice = trainingPrice;
	}

	@Override
	public String toString() {
		return "MemberPlan [id=" + id + ", price=" + price + ", freeSession=" + freeSession + ", freeTraining="
				+ freeTraining + ", type=" + type + ", sessionPrice=" + sessionPrice + ", trainingPrice="
				+ trainingPrice + "]";
	}
	
	@Override
	public String getAuthority() {
		
		return type;
	}
	
	
}