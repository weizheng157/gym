package com.mercury.member.beans;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


@Entity
@Table(name = "MEMBER")
public class Member implements UserDetails{
	@Id
	@SequenceGenerator(name = "member_seq_gen", sequenceName = "MEMBER_SEQ", allocationSize = 1)
	@GeneratedValue(generator = "member_seq_gen", strategy = GenerationType.AUTO)
	private int id;
	@Column
	private String username;
	@Column
	private String password;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
	@JoinTable(
		name = "MEMBER_PROFILE",
		joinColumns = {
			@JoinColumn(name = "MEMBER_ID", referencedColumnName = "ID")
		},
		inverseJoinColumns = {
			@JoinColumn(name = "PLAN_ID", referencedColumnName = "ID")
		}			
	)
	private List<MemberPlan> memberPlan;
	
	@OneToOne(mappedBy="member", cascade = CascadeType.ALL)
	private MemberDetail memberDetail;
	
	
	public Member() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Member(int id, String username, String password, List<MemberPlan> memberPlan, MemberDetail memberDetail) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.memberPlan = memberPlan;
		this.memberDetail = memberDetail;
	}
	
	@Override
	public String toString() {
		return "Member [id=" + id + ", username=" + username + ", password=" + password + ", memberPlan=" + memberPlan
				+ ", memberDetail=" + memberDetail + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<MemberPlan> getMemberPlan() {
		return memberPlan;
	}

	public void setMemberPlan(List<MemberPlan> memberPlan) {
		this.memberPlan = memberPlan;
	}

	public MemberDetail getMemberDetail() {
		return memberDetail;
	}

	public void setMemberDetail(MemberDetail memberDetail) {
		this.memberDetail = memberDetail;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return memberPlan;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
